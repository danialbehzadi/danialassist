#! /usr/bin/env python3
# Danial Behzadi - 2018
# Released under GPLV3+

from pyrogram import Client
from pyrogram.api import types
from pyrogram.api.functions import contacts

def create_contacts_ids():
    contacts_list = client.send(contacts.GetContacts(0)).contacts
    with open("contacts", "w") as file:
        for contact in contacts_list:
            file.write(str(contact.user_id))
            file.write("\n")

def is_contact(contact_id, self_call=0):
    with open("contacts", "r") as file:
        for cid in file.readlines():
            if contact_id == int(cid):
                return True
    if self_call == 0:
        create_contacts_ids()
        if is_contact(contact_id, self_call=1):
            return True
    return False

def update_handler(client, update, users, chats):
    if isinstance(update, types.UpdateNewMessage): # Filter by UpdateNewMessage (Private Messages)
        message = update.message
        sender_id = message.from_id
        if isinstance(message, types.Message): # Filter by Message to exclude MessageService and MessageEmpty
            if isinstance(message.to_id, types.PeerUser): # Private Messages (Message from user)
                if not is_contact(sender_id):
                    client.send_message(chat_id=sender_id, text="درود. این پیام خوانده نخواهد شد. لطفاً به جای ارسال پیام در تلگرام، از رایانامه استفاده کنید. در صورتی که فکر می‌کنید اشتباهی رخ داده، با من تماس بگیرید. نشانی رایانامه: dani.behzi@ubuntu.com")

client = Client(session_name="danialassist")
client.set_update_handler(update_handler)
client.start()
create_contacts_ids()
client.idle()
